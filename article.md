## Data Scientists, Stand out by Sharing Your Notebooks

Data Science has been dubbed as the sexiest profession of the 21st century. Demand is at an all-time high. Salaries are — particularly when Machine and Deep Learning are thrown into the mix — well into the six figures.

That’s great, but there is one small problem. Competition for available positions is fierce. Especially if you are starting out and don’t have years of experience to sell your case to prospective employers. So how do you stand out and get your foot into this industry?

As someone who has recruited, interviewed, and hired countless developers and data scientists over the years, I can tell you that it does not actually take that much to stand out. So in this article, I am going to focus on a simple strategy that you can implement right away to improve your odds of getting hired and grow your presence in this industry.

**bold**
_italic_
[link](www.google.com)
![Git Hub Share](images/shareongithub.png)
