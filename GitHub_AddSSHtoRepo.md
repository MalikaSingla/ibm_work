### Adding SSH Key to GitHub

<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

<b>Effort:</b> 15 min

## Objective:
Adding an SSH key to GitHub.


## Pre-requisites
This hands-on lab requires you to generate SSH key, as illustrated in the [previous lab](SSH_Key.md.html)

## Steps to Add SSH Key

Open GitBash to copy the SSH key previously generated.

Step 1: Copy the command using the below command:

`cat ~/.ssh/id_rsa.pub | clip`

<b>NOTE:</b> If clip doesn't work, use `cat ~/.ssh/id_rsa.pub` on command line and the copy the output.


Step 2: Now, Open GitHub and go to `Setting` (Click on the arrow)

![image](images/settings.png)

Step 3: Under Personal settings, select `SSH and GPG keys`, as shown below:

![image](images/SSHKey_option.png)

Step 4: Click the button to add a `New SSH key`.

![image](images/AddNewSSH.png)

Step 5: Provide the title. Then select the Key field, and press Ctrl-v to paste the key from the clipboard buffer. The pasted key should have <b>your email address</b> at the end, as shown below:

![image](images/add_ssh_keytoaccount.png)

and click `Add SSH Key`.

Now, the SSH key is added to your account.

## Summary
In this document, you have learned how to add SSH key to GitHub.

## Author(s)
<h4> Malika Singla <h4/>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-07-16 | 0.2 | Malika Singla | Spell check, screenshot updated and steps added |
| 2020-07-15 | 0.1 | Malika Singla | Initial version created |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
