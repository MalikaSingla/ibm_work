<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Peer-graded Assignment Part 2- Creating Visualizations Using Cognos Analytics

**Estimated time needed: 45 minutes**

In this hands-on lab you will create visualizations and add them to a dashboard using Cognos Analytics.

### Software Used in this Assignment

The hands-on lab in this assignment will use the trial version of Cognos Analytics from IBM.

### Dataset Used in this Assignment

The dataset used in this lab comes from the following source: https://community.ibm.com/accelerators/?context=analytics&type=Data&product=Cognos%20Analytics&industry=Banking in the IBM Accelerator Catalog. The Terms of use for such are located at https://developer.ibm.com/terms/ibm-developer-terms-of-use/.

### Accessing the Dataset Used in this Assignment

1.	The dataset used in this final assignment is provided as sample data within your Cognos environment, in a data module called **Incentive Compensation Management Data Module**.

To load and open the data module:

On the navigation panel of Cognos Analytics, select **Team content** and then select **Samples**.

![image](images/Team_Content.png)

2.	Now, select Go By **Industry > Banking > Data > Incentive Compensation Management Data Module**.

3.	Here, the sample data used in this final assignment can be found, in a data module called **Incentive Compensation Management Data Module**. Right-click **Incentive Compensation Management Data Module** and select **Create Dashboard**.

![image](images/Incentive_data_module.png)

### Guidelines for Submission

Use the course videos and hands-on lab from Module 2 **Creating Dashboards Using IBM Cognos Analytics** to help you complete these tasks.

Tasks to perform:

•	Create one dashboard using the 2 x 2 rectangle areas tabbed template. Rename this dashboard tab to **Compensation on Period**.

![image](images/grid4.png)

•	Create one dashboard using the tabbed template that has 2 small rectangles at the top and a large rectangle below. Rename this dashboard tab to **Risk Analysis**.

![image](images/grid3.png)

On the **Compensation on Period** dashboard, <ins>capture the following KPI metrics as visualizations:</ins>

1) In the first small rectangle (Panel 1), capture the **Risk Score** and format it to 2 decimals.

2) In the second small rectangle (Panel 2), capture the **Branch Average**.

3) In the third small rectangle (Panel 3), create a bar chart that captures **Total Incentive by Period**. Display the values on the bar chart with no grids.

4) In the fourth small rectangle (Panel 4), create a line chart that captures **Branch Average by Period**. Display values on the line chart with no grids.

On the **Risk Analysis** dashboard, capture the following KPI metrics as visualizations:

1)	In the top left area (Panel 1), create a scatter plot that captures **Region by Complaints** with points for **Education** and **Region Name** and then title the plot **Complaints by Customer Advisor Education Level**. (Hint: Use _Region_ on the x-axis, _Complaints_ on the y-axis, and use _Education_ and _Region Name_ for points.)

2)	In the top right area (Panel 2), create a line chart that captures **Protected Customers by Period**, colored by **Region ID** and **Region Name**, and then title the chart **Protected Customer Transactions by Year**. (Hint: Use _Period_ for the x-axis and _Protected Customers_ for the y-axis and then color it with _Region ID_ and _Region Name_.)

3)	In the bottom left area (Panel 3), create a scatter plot that captures **Total Incentive by Risk Score** with **Branch** for points and color. Then, title the plot **Metric by Calendar Year**. (Hint: Use _Total Incentive_ for the x-axis and _Risk Score_ for the y-axis, and _Branch_ for color and points.)

### To export your dashboard as a PDF, follow the instructions below:

1.	On the application toolbar of your dashboard page, click the _Share_ icon.

![image](images/share.png)

2.	On the **Export** tab, check **Include filters** and the click **Export**

![image](images/export.png)

3.	On the Print pop-up page, in the **Destination** drop-down list, select **Save as PDF**.

![image](images/savepdf.png)

4.	Save the PDF file on your local machine to any location you like (preferably your Downloads folder) for later upload and submission to the Coursera platform.

### Grading Information

For your assignment to be graded in a subsequent step in the course, you will be required to upload the PDFs of your Cognos Analytics dashboards that you exported to your Downloads folder in Task 4. You will upload these exported PDFs to the edX platform as part of your submission. (Important: If you cannot export your dashboards as PDFs for any reason, then you must take screenshots of your two dashboards, and submit these for grading instead).

## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Malika Singla, Himanshu Birla

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-03-16 | 1.0 | Malika | Created the version on GitLab|


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
