<img src="./images/IDSNlogo.png" width="200" height="200"/>

## Peer-graded Assignment Part 1 – Creating Visualizations Using Excel

**Estimated time needed: 45 minutes** 

In this hands-on lab you will create visualizations using Excel for the web.

### Software Used in this Assignment

The instruction videos in this course use the full Excel Desktop version as this has all the available product features, but for the hands-on labs we will be using the free **Excel for the web** version as this is available to everyone.

Although you can use the Excel Desktop software if you have access to this version, <ins>it is recommended that you use Excel for the web</ins> for the hands-on labs as the lab instructions specifically refer to this version, and there are some small differences in the interface and available features. 

### Dataset Used in this Assignment

The data set used in this lab comes from the following source: https://community.ibm.com/accelerators/?context=analytics&type=Data&product=Cognos%20Analytics&industry=Banking in the IBM Accelerator Catalog. The Terms of use for such are located at https://developer.ibm.com/terms/ibm-developer-terms-of-use/.

We are using a modified subset of the dataset for the lab. In order to follow the lab instructions successfully, please use the dataset provided with the lab, rather than the dataset from the original source. 

### Guidelines for Submission
Download the file [Incentive_Compensation_Management_Conduct_Risk- Start.xlsx](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DV0130EN-SkillsNetwork/edX_final/data/Incentive-Compensation-Management-Conduct-Risk-Start.xlsx). Upload and open the file in Excel for the web.

Use the course videos and hands-on lab from Module 1 **Creating Charts** to help you complete these tasks.

### Tasks to perform:

1.	**Total Incentives by Branch** - as a bar chart, sorted in either ascending or descending order of Total Incentives, and change the chart title to **Total Incentives by Branch**. (Hint: Use the pivot table on Sheet1 to create the chart, and use _Format_ on the _Chart_ tab to change the chart title.)

2.	**Risk Score and Total Incentives by Period Description**- as a line chart, and give the chart a title of **Risk Score and Incentives by Period**. (Hint: Use the pivot table on Sheet2 to create the chart, and use _Chart Title_ on the _Chart_ tab to add the chart title.)

3.	**Complaints by Period description using Branch** as filter – as a column chart and give the chart a title of **Complaints by Period for All Branches**. Use the filter for Period Description to show complaints for only October and November. (Hint: Use the pivot table on Sheet3 to create the chart, and use _Chart Title_ on the _Chart_ tab to change the chart title.)

4.	**Total Incentives by Region Name and Branch ID** – as a column chart and give the chart a title of **Incentives by Region Name and Branch**. Also, remove the horizontal gridlines from the chart, put the legend on the left side of the chart, and color the series outline in green. (Hint: Use the pivot table on Sheet4 to create the chart, use _Gridlines_ in the _Axes_ group on the _Chart_ tab to remove the gridlines, and use _Format_ on the _Chart_ tab to change the chart title, move the legend, and format the _series_ outline color in green.)

5.	**Save your workbook**: Use _Save As_ to save your completed workbook as **Incentive_Compensation_Management_Conduct_Risk - End.xlsx**.

## Author(s)
<h4> Rav Ahuja <h4/>

### Other Contributor(s) 
Malika Singla, Himanshu Birla

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-03-16 | 1.0 | Malika | Created the version on GitLab|


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
