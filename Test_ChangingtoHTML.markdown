<div class="item-box-content">
<div><div class="reading-title">
<h2 class="_1l2q8kho reading-header m-b-1s">Exercise: Identifying objects in images with IBM Watson</h2>
<hr class="_1ixrep9" style="margin-left: 0px; margin-bottom: 24px;"></div>
<div id="" class="rc-CML styled" dir="auto"><div><div data-track="true" data-track-app="open_course_home" data-track-page="item_layout" data-track-action="click" data-track-component="cml" role="presentation">
<div data-track="true" data-track-app="open_course_home" data-track-page="item_layout" data-track-action="click" data-track-component="cml_link">

<div>
<h2>About this exercise:</h2>
<p>In this lesson, we'd like to take you on a bit of a side journey. It's a fun exercise and we hope you enjoy it as much as we enjoyed putting this short exercise together for you. This exercise is ungraded, so feel free to have fun with it!</p><h2>Image Classification with IBM Watson Visual Recognition:</h2>
<p>What better way to understand the applications of data science than to try it out yourself? You'll be uploading images and seeing how IBM Watson identifies the various objects and faces (even gender and age!) in your images.</p>
<p></p>
<figure><img src="https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/mboZx__UEeiTKQ5ajE7PqA_b6a800499542e2907fa2626ae0a2f714_Screen-Shot-2018-12-14-at-2.15.09-PM.png?expiry=1598140800000&amp;hmac=JS_9wtmxL6WDOB-GXcxq-P4yEVKrjd8TL82Pw0z4Ajw" alt="undefined" data-asset-id="mboZx__UEeiTKQ5ajE7PqA"></figure>
<p></p>
<p>To learn how to classify your own images, continue on to the next reading!</p>
<p></p>
</div>
</div>
</div>
</div>
</div>
<div class="rc-ReadingCompleteButton horizontal-box align-items-right">