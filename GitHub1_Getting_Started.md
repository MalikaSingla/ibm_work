## Hands-on Lab: Getting Started with GitHub
<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

<b>Effort:</b> 20 min

In this lab, you will get started with GitHub by creating a GitHub account and project and adding a file to it using its Web interface.

## Objectives
After completing this lab, you will be able to:

0.  Describe GitHub
1.	Create a GitHub account
2.	Add a Project / Repo
3.	Edit / Create a file
4.  Upload a file & Commit

## GitHub Overview
First, let us introduce to GitHub. GitHub in simple words is a collection of folders and files. It is a Git repository hosting service, but it adds many of its own features. While Git is a command-line tool and a server needs to be hosted and maintained via command line as well, GitHub provides this Git server for you and a Web-based graphical interface. It also provides access control and several collaboration features, such as wikis and basic task management tools for every project. GitHub provides cloud storage for source code, supports all popular programming languages, and streamlines the iteration process.  GitHub includes a free plan for individual developers and for hosting open source projects.

## Exercise 1: Creating a GitHub Account

Please follow the steps given below to create an account in GitHub:

Step 1: Create an account: https://github.com/join 

<b>NOTE:</b> If you already have a GitHub account, you can skip this step and simply login to your account.

Step 2: Provide the necessary details to create an account as shown below:

![image](images/Create_account.PNG)

and click 'Create account'.

Step 3: Click `Verify` to verify the account and click 'Done' 

![image](images/Verify.PNG "Verify")

Step 4: After verification, click `Join a Free Plan`

![image](images/JoinFreePlan.PNG "Join Free Plan")

Step 5: Select the details as shown below and click `Complete Setup`

![image](images/CompleteSetup.PNG "Complete Setup")

Step 6: Go to your email,  find the verification email from GitHub, and click on the link/button in that email to verify your email. 

<b>NOTE:</b> If you will not receive verification email, click `Resend verification email`.

![image](images/VerifyEmailAddress.PNG "Verify Email Address")

Email is verified

![image](images/EmailVerified.png "Email Verified")

## Exercise 2: Adding a Project / Repo

Step 1: Click on the `+` symbol and click `New repository`.

![image](images/Create_repo.png "New Repo")

Step 2: Provide a repository a name and initialize with the empty `README.md` file.

![image](images/Repo_details.png "Repo Details")

and click `Create repository`.


Now, you will be redirected to the repository you have created.

Let's start editing the repository.

## Exercise 3: Create / edit a file

### Exercise 3a: Edit a file 

Step 1: Once the repository is created, the root folder of your repository is listed by default and it has just one file `ReadMe.md`. Click on the pencil icon to edit the file.

![image](images/Edit_file.png "Edit File")

Step 2: Add text to file.

![image](images/Add_text.png "Add Text")

Step 3: Scroll down the page after adding the text and click `Commit Changes`.

![image](images/Commit.png "Commit")

Now, check your file is edited with the new text.

### Exercise 3b: Create a new file

Step 1: Click on the repository name to go back to the master branch like in this testrepo.

![image](images/Add_file1.png "Add_file1")

Step 2: Click `Add file` and select `Create New file` to create a file in the repository.

![image](images/Create_new_file.png "Create New File")

Step 3: Provide the file name and the extension of the file. For example, firstpython.py and add the lines.

![image](images/Add_filename.png "Add filename")

Step 4: Scroll down the page after adding the text. `Add description` of the file (optional) and click `Commit` new file.

![image](images/commit_newfile.png "Commit New file")

Step 5: Your file is now added to your repository and the repository listing shows when the file was added/changed.


## Exercise 4: Upload a file & Commit

Step 1: Click `Add file` and select `Upload files` to upload a file in the repository from the local computer.

![image](images/upload.png "Upload")

Step 2: Click on choose your files and choose any files from your computer.

![image](images/select_files.png "Select files")

Step 3: Once the file finishes uploading, click on `Commit` Changes

![image](images/commit_upload.png "Commit Uplaod")

Step 4: Now, your file is uploaded in the repository.

![image](images/check_change.png "Check change")

## Summary
In this document, you have learned how to create a new repository, adding a new file, editing a file, and uploading a file in a repository and commit the changes.

## Author(s)
<h4> Romeo Kienzler <h4/>
<h4> Malika Singla <h4/>

### Other Contributor(s) 
Rav Ahuja

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-07-16 | 0.5 | Malika Singla | Spell check, step number added |
| 2020-07-14 | 0.4 | Rav Ahuja | Changed logo, updated effort, title, intro, objectives, added Authors and Changelog |
| 2020-07-13 | 0.3 | Malika Singla | Added to GitLab and made some formatting changes, added objectives, etc. |
| 2020-07-03 | 0.2 | Malika Singla | Ported to markdown and added GitHub account signup, new screenshots, etc. |
| 2020-06-30 | 0.1 | Romeo Kienzler | Drafted initial version |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
