<img src="images/IDSNlogo.png" width="200" height="200"/>

## Objectives

### Hands on Lab: IBM Cloud Service Creation(10min)

Objective for Exercise:
* How to create an IBM Cloud account.
* How to create a Watson Studio Service instance.

> **Note:** A credit card is NOT required to sign up for an IBM Cloud Lite account and there is no charge associated in creating a Lite plan instance. 

 
### Exercise 1: Create an IBM Cloud Account

#### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. 

If you already have an IBM Cloud account, you can skip Exercise 1, login to your IBM cloud account and proceed to **Exercise 2: Add Watson Studio as a resource.**

#### Task 1: Sign up for IBM Cloud

1.  Go to: [IBM Cloud Registration](https://cloud.ibm.com/registration) to create a free account on IBM Cloud.

2. Enter your company **Email** address and a strong **Password** and then click the **Next** button.
        <img src="images/1-email.jpg"  width="600">

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.
        <img src="images/2-verify_email.jpg"  width="600">

4. Once your email is successfully verified, enter your **First name**, **Last name**, and **Country** and click **Next**.
        <img src="images/3-personal_info.jpg"  width="600">

5. After your personal information is accepted Click on **Create account** button.
        <img src="images/4-create_account.jpg"  width="600">

        >*It takes a few seconds to create and set up your account.*

6. You will see this page which confirms your account creation and allows you to login.
        <img src="images/account_setup_screen.png"  width="600">


7. The username (which is your email address) is already populated. Enter your password and login.
        <img src="images/login_screen.png"  width="600">

8. Read carefully about the IBMid Privacy and click on proceed to acknowledge and login.
        <img src="images/account_privacy.png"  width="600">

9. Once you successfully login, you should see the dashboard. You can now proceed to the next exercise.
        <img src="images/dashboard.png"  width="600">


### Exercise 2: Create an instance of Watson Studio service 

#### Task 1: Add Watson Studio as a resource

1. Go to [IBM Cloud Login](https://cloud.ibm.com/login) and login with your credentials.

2. In the [IBM Cloud Catalog](https://cloud.ibm.com/catalog) search or choose to the Watson Studio listing.
        <img src="images/Catalog_WS.png"  width="75%"/>

3. On the Watson Studio page, select an appropriate region depending on where you are accessing from, verify that the **Lite** plan is selected, and then click **Create**.
        <img src="images/5-create_watson_studio_lite.jpg"  width="75%"/>


4. Once the Watson Studio instance is successfully created, click on **Get Started**.
        <img src="images/6-watson_studio_get_started.jpg"  width="75%"/>

5. Once you get started, the first time, your IBM Cloud Pak for Data core services is provisioned.
        <img src="images/provisioning.png"  width="75%"/>




## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-06-03 | 2.0 | Srishti | Created lab |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
