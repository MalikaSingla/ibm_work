<img src="images/IDSNlogo.png" width="200" height="200"/>

# Project Scenario
You are the hotel manager and want to learn what the customers think about their stay in your hotel. You will use Machine Learning - Modeler Flow to do some analysis on the hotel satisfaction review dataset.

# Project Overview

**Pre requisites:**
1. Create an IBM cloud account if you don't have one already.
2. Create an instance of the Watson Studio service.
>Note: If you don't have these, please go to this [link](AI In Action Part 1 lab Link Here) and  create the account and service.


Next step involves the below tasks:

**Task 1:** Create a project on Watson Studio

**Task 2:** Explore Hotel Satisfaction Model

**Task 3:** Run Hotel Satisfaction Model

**Task 4:** Check Out The Results!

Each of the above tasks will be described in more detail in the sections that follow.

## Next Steps
To complete the Hands-on lab, follow the step-by-step instructions.

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-06-08 | 2.0 | Srishti | Added Project Overview |
