<img src="images/IDSNlogo.png" width="200" height="200"/>

## Hands on lab: Classify Hotel Satisfaction data using a Data Modeler (30min)

### Lab overview:

IBM Modeler Flow is a service that speeds up creation of machine learning algorithms. In this hands-on lab, you will use Modeler Flow to do some analysis to help the hotel manager learn what customers think about the hotel. This example uses Text Analytics nodes to analyze fictional text data about hotel personnel, comfort, cleanliness, price, etc. 

### Objective:
Objective for Exercise:
* How to create a project in Watson Studio
* Understand Data Modeler.
* Learn how to classify hotel reviews using a Data Modeler.

### Pre-requisite
To complete this lab, you must have created an IBM Cloud account and Watson Studio service which is instructed in previous lab: [Hands on Assignment: IBM Cloud Service Creation](Paste Asset libraby link)


### Exercise 1: Create a project on Watson Studio

#### Task 1: Create an empty project

1. On the Watson Studio Welcome page, click **Create a project**.

<img src="images/7-create_project.jpg"  width="600" style="border: solid 1px grey"/>

2. On the Create a project page, click **Create an empty project**.

<img src="images/8-create_empty_project.jpg"  width="600" style="border: solid 1px grey"/>


3. On the New project page, enter a **Name** and **Description** for your project.

<img src="images/projectname.png"  width="600" style="border: solid 1px grey"/>


4. You must define storage for your project before you can create it. If you already have an instance of Cloud Object Storage, you can select it, otherwise under **Select storage service**, click **Add**.

<img src="images/10-add_storage.jpg"  width="600" style="border: solid 1px grey"/>


5. On the Cloud Object Storage page, verify that **Lite** plan is selected, and then click **Create**.

<img src="images/11-create_cos.jpg"  width="600" style="border: solid 1px grey"/>


6. Now on the Create Project page, under **Define storage**, you may need to click **Refresh** to see the newly created instance of Cloud Object Storage (COS).

<img src="images/12-refresh_storage.jpg"  width="600" style="border: solid 1px grey"/>


7. Once an instance of Cloud Object Storage (COS) is listed, click **Create** to create the Project.
<img src="images/13-create_project_with_cos.jpg"  width="600" style="border: solid 1px grey"/>

8. A project should now be created in Watson Studio.
<img src="images/14-project_created.png"  width="600" style="border: solid 1px grey"/>

9. Click on **Add to project** and in **Choose Asset Type** box select **Modeler flow** from the list:

<img src="images/27.jpg" width="600" style="border: solid 1px grey"/>

10. We will be using an existing example, so for that on the next screen, choose **From Example**, then choose **Hotel Satisfaction Model** from the options

<img src="images/1.1.png" width="600" style="border: solid 1px grey"/>

11. It would take few seconds to load the hotel satisfaction model.

<img src="images/Creating_modeler_flow.PNG" width="600" style="border: solid 1px grey"/>

### Exercise 2: Hotel Satisfaction Model

#### Task 1: Explore Hotel Satisfaction Model: 

After creating your project, by default, you will land on the page where you can view, and explore the hotel satisfaction model. Normally you would create this model from scratch but in this case an example model flow is already provided.

At this point, you’ve just set up the model flow. It’s a visual representation of the path the data will take and what decisions will be made at each node in order to determine the sentiment from hotel review. That is, this is the model that we’ll feed data to in order to get a **Category data** and **Concept data**. 

1. Right click the purple box, labeled **hotel satisfaction**, and choose **Preview**.

<img src="images/1.22.png"  width="800" style="border: solid 1px grey"/>

The preview button shows data set before you actually run it through the model. This is the training data. We’ll get the result once we run the model.

<img src="images/1.222.PNG"  width="800" style="border: solid 1px grey"/>


#### Task 2: Run Hotel Satisfaction Model

Now that you’ve explored the model and have begun to understand the underlying data on how the model flow from hotel satisfaction dataset to sentiment(Distribution) by generating Positive/Negative, through **CountSentiments** and **DeriveSentiments**. 

Sentiment analysis (or opinion mining) uses natural language processing and machine learning to interpret and classify emotions in subjective data.
It's a natural language processing algorithm that gives you a general idea about the positive, neutral, and negative sentiment of texts.

1. Now we will run the model to view and analyze the outputs.

    Towards the top of the screen, press the **Play** button (<img src="images/playback.jpg"  width="20">), the model should render.

<img src="images/Run.png"  width="600" style="border: solid 1px grey"/>

#### Task 3: Check Out The Results!

Once you have run the model, Watson Machine Learning will create new Categories and concept data.

It uses Text Analytics nodes to analyze fictional text data about hotel personnel, comfort, cleanliness, price, etc. The Text Analytics nodes offer powerful text analytic capabilities, which use advanced linguistic technologies and Natural Language Processing (NLP) to rapidly process a large variety of unstructured text data and, from this text, extract and organize the key concepts. Text Analytics can also group these concepts into categories.

<img src="images/H1.PNG"  width="800" style="border: solid 1px grey"/>

##### Category Data
In the Categories pane you can build and manage your categories. This pane is located in the upper left corner of the Categories and concepts view. 

Since this example flow is built using a text analysis package (TAP), the category model is already populated.

Each time a category is created or updated, you can see whether any text matches a descriptor in a given category by clicking Score to score the documents or records. If a match is found, the document or record is assigned to that category. 

You can expand each category sub-category, select them or a descriptor, and click Display to see the source data:

Select **All** and click on **Display**, then analyze the result from the **preview**

<img src="images/1.44.png"  width="800" style="border: solid 1px grey"/>

##### Concept Data

During the extraction process, the text data is analyzed to identify interesting or relevant single words such as airport, location, and word phrases such as airport pick-up. These words and phrases are collectively referred to as terms. Using the linguistic resources, the relevant terms are extracted, and similar terms are grouped together under a lead term called a concept.

In this way, a concept might represent multiple underlying terms depending on your text and the set of linguistic resources you're using.

<img src="images/1.55.png"  width="800" style="border: solid 1px grey"/>

You can also use a Filter to select a subset of concepts, and you can select from different options:

<img src="images/4.PNG"  width="800" style="border: solid 1px grey"/>

<img src="images/5.PNG"  width="800" style="border: solid 1px grey"/>

##### Text link analysis

Text Link analysis, identify relationships between the concepts in the text data based on known patterns.

1. Access the text links from the Text link analysis pane:

<img src="images/6.111.png"  width="800" style="border: solid 1px grey"/>

2. Select a Type Pattern from the top pane (for example, <Services> <Positive>) to display the corresponding Concepts Patterns in the bottom pane. To see the corresponding text, click Display. As the text in the Comments cell may be truncated, you can click on a cell to display the entire text in the Highlighted cell.

<img src="images/7.11.PNG"  width="800" style="border: solid 1px grey"/>

## Summary

In this lab, you explored the key takeaways: 
- Learnt how to add modeler flow to your Watson Studio project
- Learnt how to run and train model on modeler flow.
- Learnt how to explore the result. 

## Author(s)
[Srishti Srivastava](https://www.linkedin.com/in/srishti-srivastava-343095a8/)


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-08-27 | 2.0 | Srishti | Created Modeler flow lab |
| 2021-06-04 | 2.1 | Srishti | Updated steps |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
