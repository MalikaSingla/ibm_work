<img src="images/IDSNlogo.png" width="200" height="200"/>

## Objectives

### Hands on Lab: Explore Watson Gallery (30 min)

#### Objective for Exercise:
* Access Watson Studio
* Explore Gallery 
    - Explore Data
    - Explore Notebook 


#### Pre-requisite: 
Before you start, you need to have an IBM Cloud account. If not, follow the instructions given in the [link](C:\Users\Skill07\Desktop\ModelerFlow\Watson Studio Setup.md)


### Exercise 1: Access Watson Studio

1. Login to IBM Cloud: https://cloud.ibm.com/login

2. Scroll down and click _Services_ given in _Resource Summary_.

<img src="images/Services.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>



3. When you click on Services, all your existing services will be shown in the list. Click the Watson Studio service you created:

<img src="images/Watson_Studio.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

4. Click _Get Started_.

<img src="images/Get_Started.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>


## Exercise 2: Explore Gallery (For Data, Visualization)

1. Click on Navigation Menu.

<img src="images/Navigate.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

2. Click on _Gallery_.

<img src="images/Gallery.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

### Task 2A: Explore Data

Let's first explore the data with is based on numbers. 

1. Select _All Filters_. From _Format_ select _Data_ and from _Topic_ select _Energy & Utilities, Enviornment and Industry Accelerator_

<img src="images/filter_1.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

2. Click on _UCI: Forest Fires_. 

<img src="images/UCI_Forest.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

3. Preview the data using the _Preview_ option. 

<img src="images/data_UCI_Fires.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

Explore the data, the data is related to forest fires where the aim is to predict the burned area of forest fires, in the northeast region of Portugal, by using meterological and other data. 

**Attribute Information:**

1. X - x-axis spatial coordinate within the Montesinho park map: 1 to 9
2. Y - y-axis spatial coordinate within the Montesinho park map: 2 to 9
3. month - month of the year: 'jan' to 'dec'
4. day - day of the week: 'mon' to 'sun'
5. FFMC - FFMC index from the FWI system: 18.7 to 96.20
6. DMC - DMC index from the FWI system: 1.1 to 291.3
7. DC - DC index from the FWI system: 7.9 to 860.6
8. ISI - ISI index from the FWI system: 0.0 to 56.10
9. temp - temperature in Celsius degrees: 2.2 to 33.30
10. RH - relative humidity in %: 15.0 to 100
11. wind - wind speed in km/h: 0.40 to 9.40
12. rain - outside rain in mm/m2 : 0.0 to 6.4
13. area - the burned area of the forest (in ha): 0.00 to 1090.84
(this output variable is very skewed towards 0.0, thus it may make
sense to model with the logarithm transform).

The data cannot be only based on numbers, data can be text, images as well. Let's explore data having text values.

1. Use the _All Filters_. From _Format_ select _Data_ and from _Topic_ select _Economy and Business_.

You will get mutiple datasets given. Scroll down and select _Airbnb Data for Analytics: Trentino Reviews_ (If you will not get the data using **Load More** option)

<img src="images/AIrbnb.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>


2. Preview the data using the _Preview_ option.

<img src="images/Airbnb_preview.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

Explore the data, Airbnb, Inc. is an American company that operates an online marketplace for lodging, primarily homestays for vacation rentals, and tourism activities. Airbnb guests may leave a review after their stay, and these can be used as an indicator of airbnb activity.The minimum stay, price and number of reviews have been used to estimate the occupancy rate, the number of nights per year and the income per month for each listing.

Using the data Airbnb uses location data to improve guest satisfaction.

The dataset comprises of three main tables:

 - listings - Detailed listings data showing 96 attributes for each of the listings. Some of the attributes used in the analysis are price(continuous), longitude (continuous), latitude (continuous), listing_type (categorical), is_superhost (categorical), neighbourhood (categorical), ratings (continuous) among others.

- reviews - Detailed reviews given by the guests with 6 attributes. Key attributes include date (datetime), listing_id (discrete), reviewer_id (discrete) and comment (textual).

- calendar - Provides details about booking for the next year by listing. Four attributes in total including listing_id (discrete), date(datetime), available (categorical) and price (continuous).

### Task 2B: Explore Notebook

1. Use the _All Filters_. From _Format_ select _Notebook_ and select _Finding optimal locations of new stores using Decision Optimization_

<img src="images/New_Store.png" style="border: solid 1px grey;margin-top: 30px; margin-left: 30px; margin-bottom: 30px"/>

This notebook shows you how Decision Optimization can help to prescribe decisions for a complex constrained problem using Python to help determine the optimal location for a new store. 

The objective is to minimize the total distance from libraries to coffee shops so that a book reader always gets to our coffee shop easily. It can be done by analyzing and displaying the location of the coffee shops on a map.

#### Summary

In this lab, you have learnt about how dataset looks and its not only numbers that can be used for analysis. 

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-06-010 | 1.0 | Malika Singla | Initial Version |



## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>
