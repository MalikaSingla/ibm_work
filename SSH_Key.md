## SSH Key Generation

<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

<b>Effort:</b> 30 min

## Introduction

## SSH

An SSH key is an access credential in the SSH protocol. Its function is similar to that of user names and passwords, but the keys are primarily used for automated processes.

Step 1: Launch Git Bash.
Step 2: Type the following, replacing <your email address> with the email address that is linked to your Github account, and hit Enter:

`ssh-keygen -t rsa -b 4096 -C "<your email address>"`

This will generate a new SSH key.

Step 3: Next, you will be prompted to enter a directory to save the key. I simply press Enter to accept the default location, which is a .ssh folder in the home directory. In other words, you will be able to locate the key in `~/.ssh/id_rsa`

Step 4: You will then be prompted to choose a passphrase. I prefer not to have a passphrase, so just press Enter and Enter again to confirm the empty passphrase.

Now, if you navigate to the .ssh directory, i.e., if you run the following in the Git Bash terminal,

`cd ~/.ssh`

and then,

`ls`

To list the contents of the .ssh directory, you should find “id_rsa” and “id_rsa.pub” in the list of contents, where “id_rsa” is the private version of your key and “id_rsa.pub” is the public version of your key.

Step 5: Finally, you will need to add the SSH key to the ssh-agent, which is meant to help with the authentication process. To do that, first you need to start the ssh-agent, so run the following in the Git Bash terminal:

`eval "$(ssh-agent -s)"`

Step 6: And then add the key to the agent by running the following in the Git Bash terminal:

`ssh-add ~/.ssh/id_rsa`

## Summary
In this document, you have learned how to generate the SSH key.

## Author(s)
<h4> Malika Singla <h4/>

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-07-16 | 0.2 | Malika Singla | Spell check, step number added |
| 2020-07-15 | 0.1 | Malika Singla | Initial version created |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
