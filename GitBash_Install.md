### GitBash Installation Guide

<img style="float:left;" src="images/IDSNlogo.png" width="200" height="200"/>

<b>Effort:</b> 20 min

## Introduction

## GitBash 

Git Bash for Windows is a package that includes git and bash.

Git is an open-source version control system for tracking source code changes when developing software. It keeps a commit history which allows you to revert to a stable state in case you mess up your code. Git also allows multiple developers to collaborate on the same code base.

Bash is a Unix command-line shell. The name is an acronym for the ‘Bourne-Again Shell’. It comes with useful Unix commands like cat, ssh, SCP, etc., which are not usually found on Windows.

Installation Steps:
Step 1: Open the link https://git-scm.com/downloads
Step 2: Click on the operating system to download GitBash accordingly. Click `Download` as shown below:

![image](images/Download.png)

You will be redirected to a new page and it started downloading.

![image](images/Download_start.png)

<b>NOTE:</b> If the download doesn't start, click on the option `Click here to download manually`.

Step 3: Open the folder where it is downloaded and open it.

![image](images/Run.png)

Click `Run` to Install. 

![image](images/Next1.png)

Step 4: Select the location you want to install Git Bash. I would recommend you just leave the default option as it is, and click `Next`.

![image](images/Next2.png)

Step 5: Choose the components you want to install, or you can just proceed with the default options and click `Next`.

![image](images/Next3.png)

Step 6: You can change the name of start menu folder here if you want, or just leave the default name and click `Next`.

![image](images/Next4.png)

Step 7: Select the default editor for Git to use. Choose the one you like and click `Next`.

![image](images/Next5.png)

Step 8: Choose the option you want depending on where you want to use Git and click `Next`.

![image](images/Next6.png)

Step 9: Select `Use the OpenSSL library` and click `Next`.

![image](images/Next7.png)

Step 10: Configure the Line Ending Conversions. Go with the default option `Checkout Windows-Style, commit Unix-style line endings`. Click `Next` to proceed.

![image](images/Next8.png)

Step 11: Configure the Terminal Emulator to use with Git Bash. Default option `Use MinTTY(the default terminal of MSYS2)` and click `Next`.

![image](images/Next9.png)

Step 12: Select the Default Behavior

![image](images/Next10.png)

Step 13: Select the features you want(the default options are fine) and click `Next`.

![image](images/Next11.png)

Step 14: Enable experimental options if you want. Enabling them allows you to try out newer features that are still in development

![image](images/Next12.png)

Step 15: Wait for the installation to complete and complete `Git Setup Wizard`

![image](images/WaitInstall.png)

Step 16: The Git Bash terminal will now open and you will be able to enter Git and Bash commands.

![image](images/terminal.png)

## Summary
In this document, you have learned how to install GitBash.

## Author(s)
<h4> Malika Singla <h4/>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-07-16 | 0.2 | Malika Singla | Changed logo, added effort, spell check and step number added |
| 2020-07-15 | 0.1 | Malika Singla | Initial version created |



## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
